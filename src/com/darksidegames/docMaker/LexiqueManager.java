/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.ini4j.InvalidFileFormatException;
import org.ini4j.Profile;
import org.ini4j.Wini;

import com.darksidegames.docMaker.config.InternalLang;
import com.darksidegames.docMaker.exceptions.TermNotFoundException;


/**
 * Cette classe gère le lexique utilisé dans la documentation.
 * 
 * @author winterskill
 */
public abstract class LexiqueManager
{
	/**
	 * On utilise la langue interne si il n'y a pas de langues disponibles.
	 */
	protected static boolean useInternalLocale = false;
	/**
	 * La liste des langues disponibles.
	 */
	protected static HashMap<String, Wini> locales = new HashMap<String, Wini>();
	/**
	 * La langue actuelle.
	 */
	protected static String actualLang;
	
	/**
	 * La section qui contient les termes utilisés dans la documentation générée.
	 */
	public static final String SECTION_OUTPUT = "output";
	/**
	 * La section qui contient les termes utilisés dans l'interface graphique.
	 */
	public static final String SECTION_UI = "ui";
	
	/**
	 * Initialise le lexique.
	 * Doit être appellée en début de programme.
	 * 
	 * @param lang
	 * La langue à charger comme langue actuelle.
	 * Si la langue demandée n'existe pas, chargera la langue interne.
	 * Pour charger la langue interne comme langue actuelle, mettez un String vide ("").
	 */
	public static void initLexique(String lang)
	{
		DebugManager.print("com.darksidegames.docMaker:LexiqueManager@initLexique", "Initialize lexique...");
		
		// check si le fichier de lexique existe
		File langDir = new File("locales");
		
		if (!langDir.exists() || langDir.isFile()) {
			DebugManager.print("com.darksidegames.docMaker:LexiqueManager@initLexique", "No locales dir was found, or it's a file",
					DebugManager.MESSAGE_ERROR);
			LexiqueManager.useInternalLocale = true;
		} else {
			// on scanne le contenu du dossier locales
			File[] langDirFiles = langDir.listFiles();
			
			for (int i = 0; i < langDirFiles.length; i++) {
				if (langDirFiles[i].isFile()) {
					String name = langDirFiles[i].getName();
					String[] nameSplited = name.split("\\.");
					name = nameSplited[0];
					
					try {
						Wini w = new Wini(langDirFiles[i]);
						LexiqueManager.locales.put(name, w);
					} catch (InvalidFileFormatException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
			DebugManager.print("com.darksidegames.docMaker:LexiqueManager@initLexique", LexiqueManager.locales.size()
					+ " locale(s) scanned");
			
			if (LexiqueManager.locales.isEmpty()) {
				DebugManager.print("com.darksidegames.docMaker:LexiqueManager@initLexique", "locales directory is empty.",
						DebugManager.MESSAGE_WARNING);
				LexiqueManager.useInternalLocale = true;
			}
			
			if (!lang.equals("")) {
				// si on ne veut pas de la langue interne comme langue par défaut
				if (LexiqueManager.locales.containsKey(lang)) {
					LexiqueManager.actualLang = lang;
					DebugManager.print("com.darksidegames.docMaker:LexiqueManager@initLexique", "Actual lang set to " + lang);
				} else {
					LexiqueManager.useInternalLocale = true;
					DebugManager.print("com.darksidegames.docMaker:LexiqueManager@initLexique", "Requested lang not found. Actual lang set to internal lang", DebugManager.MESSAGE_WARNING);
				}
			} else {
				// si on veut la langue interne comme langue par défaut
				LexiqueManager.useInternalLocale = true;
				DebugManager.print("com.darksidegames.docMaker:LexiqueManager@initLexique", "Actual lang set to internal lang");
			}
		}
	}
	
	/**
	 * Retourne une entrée du lexique dans la langue actuelle.
	 * 
	 * @param termname
	 * Le nom de l'entrée du lexique.
	 * @param section
	 * La section INI.
	 * @return
	 * L'entrée du lexique.
	 * @throws TermNotFoundException 
	 */
	public static String getTerm(String termname, String section) throws TermNotFoundException
	{
		Profile.Section iniSection = LexiqueManager.locales.get(LexiqueManager.actualLang).get(section);
		
		if (iniSection.containsKey(termname)) {
			return iniSection.fetch(termname);
		} else {
			DebugManager.print("com.darksidegames.docMaker:LexiqueManager@getTerm", "Term " + termname + " not found.",
					DebugManager.MESSAGE_ERROR);
			return InternalLang.get(termname, section);
		}
	}
	
	/**
	 * Retourne si une entrée du lexique existe.
	 * 
	 * @param termname
	 * Le nom de l'entrée du lexique.
	 * @param section
	 * La section INI.
	 */
	public static boolean termExists(String termname, String section)
	{
		if (section.equals(""))
			return LexiqueManager.locales.containsKey(termname);
		else {
			Profile.Section iniSection = LexiqueManager.locales.get(LexiqueManager.actualLang).get(section);
			return iniSection.containsKey(termname);
		}
	}
}
