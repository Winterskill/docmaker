/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.darksidegames.docMaker.json;

/**
 * La classe abstraite dont héritent tous les modèles JSON.
 * 
 * @author winterskill
 */
public abstract class JSONModel
{
	/**
	 * Contient la classe active. Il suffit de faire __type pour
	 * récupérer la classe active et la comparer avec {@code <CLASSE>.class} :
	 * 
	 * <pre>
	 * JSONTypedefModel m = new JSONTypedefModel();
	 * if (m.__type == JSONTypedefModel.class) {
	 *     // trucs
	 * }
	 * </pre>
	 */
	public Object __type = this.getClass();
}
