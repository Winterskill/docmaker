/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.json;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * Représente une annotation dans la documentation.
 * 
 * @author winterskill
 */
public class JSONAnnotationElementModel extends JSONModel
{
	/**
	 * Le nom de l'annotation.
	 */
	@SerializedName("name")
	public String name;
	/**
	 * La description courte.
	 */
	@SerializedName("shortDescription")
	public String shortDescription = "";
	/**
	 * La description.
	 */
	@SerializedName("description")
	public String description = "";
	/**
	 * L'auteur de l'annotation.
	 */
	@SerializedName("author")
	public String author = "";
	/**
	 * La version minimale du code dans laquelle ont peut utiliser l'annotation.
	 */
	@SerializedName("since")
	public String since = "";
	/**
	 * L'annotation est-t-elle obsolète ?
	 */
	@SerializedName("deprecated")
	public boolean deprecated = false;
	/**
	 * La liste des arguments de l'annotation.
	 */
	@SerializedName("args")
	public List<JSONAnnotationElementArgumentModel> args;
	
	public String toString()
	{
		return name;
	}
}
