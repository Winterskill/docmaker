/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.json;

import com.google.gson.annotations.SerializedName;

/**
 * Représente un argument d'une fonction ou d'une méthode dans la documentation.
 * 
 * @author winterskill
 */
public class JSONFunctionArgumentModel extends JSONModel
{
	/**
	 * Représente le nom de l'argument.
	 */
	@SerializedName("name")
	public String name;
	/**
	 * Représente le type de l'argument.
	 */
	@SerializedName("type")
	public String type;
	/**
	 * L'argument est-t-il constant ?
	 */
	@SerializedName("isConst")
	public boolean isConst = false;
	/**
	 * L'argument est-t-il une référence ?
	 */
	@SerializedName("ref")
	public boolean ref = false;
	/**
	 * L'argument est-t-il un pointeur ?
	 */
	@SerializedName("pointer")
	public boolean pointer = false;
	/**
	 * La valeur par défaut de l'argument.
	 */
	@SerializedName("defaultValue")
	public String defaultValue = "";
	/**
	 * La description de l'argument.
	 */
	@SerializedName("description")
	public String description = "";
	
	public String toString()
	{
		return name;
	}
}
