/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.json;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * Représente une méthode dans une classe dans la documentation.
 * 
 * @author winterskill
 */
public class JSONClassMethodModel extends JSONModel
{
	/**
	 * Le nom de la méthode.
	 */
	@SerializedName("name")
	public String name;
	/**
	 * Le type de retour de la méthode.
	 */
	@SerializedName("returnValue")
	public String returnValue;
	/**
	 * La méthode retourne-t-elle une référence ?
	 */
	@SerializedName("ref")
	public boolean ref = false;
	/**
	 * La méthode retourne-t-elle un pointeur ?
	 */
	@SerializedName("pointer")
	public boolean pointer = false;
	/**
	 * La méthode est-t-elle abstraite ?
	 */
	@SerializedName("isAbstract")
	public boolean isAbstract = false;
	/**
	 * La méthode est-t-elle static ?
	 */
	@SerializedName("isStatic")
	public boolean isStatic = false;
	/**
	 * La liste des arguments de la méthode.
	 */
	@SerializedName("args")
	public List<JSONFunctionArgumentModel> args;
	/**
	 * La description courte de la méthode.
	 */
	@SerializedName("shortDescription")
	public String shortDescription = "";
	/**
	 * La description de la méthode.
	 */
	@SerializedName("description")
	public String description = "";
	/**
	 * La version minimale du code dans laquelle la méthode est présente.
	 */
	@SerializedName("since")
	public String since = "";
	/**
	 * La méthode est-t-elle obsolète ?
	 */
	@SerializedName("deprecated")
	public boolean deprecated = false;
	/**
	 * Le nom de l'auteur de la méthode.
	 */
	@SerializedName("author")
	public String author = "";
	/**
	 * La portée de la méthode.
	 */
	@SerializedName("scope")
	public JSONScope scope = JSONScope.PUBLIC;
	/**
	 * La liste des annotations présentes sur la méthode.
	 */
	@SerializedName("annotations")
	public List<JSONAnnotationModel> annotations;
	/**
	 * La méthode est-t-elle un constructeur ?
	 */
	@SerializedName("isConstructor")
	public boolean isConstructor = false;
	/**
	 * La méthode est-t-elle un destructeur ?
	 */
	@SerializedName("isDestructor")
	public boolean isDestructor = false;
	/**
	 * La liste des throws jetés par la méthode.
	 */
	@SerializedName("throwsList")
	public List<JSONFunctionThrowsModel> throwsList;
	/**
	 * La fonction est-t-elle un template ?
	 */
	@SerializedName("template")
	public boolean template = false;
	/**
	 * La liste des arguments du template.
	 * Pris en compte uniquement si template=true.
	 */
	@SerializedName("template_args")
	public List<JSONTemplateArgumentModel> template_args;
	
	public String toString()
	{
		return name;
	}
}
