/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.json;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * Représente une fonction dans la documentation.
 * 
 * @author winterskill
 */
public class JSONFunctionModel extends JSONModel
{
	/**
	 * Le nom de la fonction.
	 */
	@SerializedName("name")
	public String name;
	/**
	 * Le type de valeur retournée par la fonction.
	 */
	@SerializedName("returnValue")
	public String returnValue;
	/**
	 * La fonction retourne-t-elle une référence ?
	 */
	@SerializedName("ref")
	public boolean ref = false;
	/**
	 * La fonction retourne-t-elle un pointeur ?
	 */
	@SerializedName("pointer")
	public boolean pointer = false;
	/**
	 * La liste des arguments de la fonction.
	 */
	@SerializedName("args")
	public List<JSONFunctionArgumentModel> args;
	/**
	 * La description courte de la fonction.
	 */
	@SerializedName("shortDescription")
	public String shortDescription = "";
	/**
	 * La description longue de la fonction.
	 */
	@SerializedName("description")
	public String description = "";
	/**
	 * La version minimale du code où la fonction est présente.
	 */
	@SerializedName("since")
	public String since = "";
	/**
	 * La fonction est-t-elle obsolète ?
	 */
	@SerializedName("deprecated")
	public boolean deprecated = false;
	/**
	 * Le nom de l'auteur de la fonction.
	 */
	@SerializedName("author")
	public String author = "";
	/**
	 * La liste des annotations présentes sur la fonction.
	 */
	@SerializedName("annotations")
	public List<JSONAnnotationModel> annotations;
	/**
	 * La liste des throws de la fonction.
	 */
	@SerializedName("throwsList")
	public List<JSONFunctionThrowsModel> throwsList;
	/**
	 * La fonction est-t-elle un template ?
	 */
	@SerializedName("template")
	public boolean template = false;
	/**
	 * La liste des arguments du template.
	 * Pris en compte uniquement si template=true.
	 */
	@SerializedName("template_args")
	public List<JSONTemplateArgumentModel> template_args;
	
	public String toString()
	{
		return name;
	}
}
