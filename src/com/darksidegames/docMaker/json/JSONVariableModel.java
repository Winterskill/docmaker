/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.json;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * Représente une variable ou une constante dans la documentation.
 * 
 * @author winterskill
 */
public class JSONVariableModel extends JSONModel
{
	/**
	 * Le nom de la variable.
	 */
	@SerializedName("name")
	public String name;
	/**
	 * Le type de la variable.
	 */
	@SerializedName("type")
	public String type;
	/**
	 * La variable est-t-elle un pointeur ?
	 */
	@SerializedName("pointer")
	public boolean pointer = false;
	/**
	 * La variable est-t-elle une constante ?
	 */
	@SerializedName("isConst")
	public boolean isConst = false;
	/**
	 * La description de la variable.
	 */
	@SerializedName("description")
	public String description = "";
	/**
	 * L'auteur de la variable.
	 */
	@SerializedName("author")
	public String author = "";
	/**
	 * La version minimale du code dans laquelle on peut voir la variable.
	 */
	@SerializedName("since")
	public String since = "";
	/**
	 * La variable est-t-elle obsolète ?
	 */
	@SerializedName("deprecated")
	public boolean deprecated = false;
	/**
	 * La liste des annotations sur la variable.
	 */
	@SerializedName("annotations")
	public List<JSONAnnotationModel> annotations;
	
	public String toString()
	{
		return name;
	}
}
