/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.entryPoint;

/*
 * TODO maintenant que les chemins du classpath sont relatifs, il est peut-être
 * possible de run la version .exe du programme sans les libs externes...
 * Réponse : non
 */
/*
 * TODO écrire la doc du programme (expliquant la structure des fichiers JSON.
 */
// TODO Crérer une icone pour l'application
// TODO ajouter une icone à la fenêtre LicenceWindow
// TODO ajouter une icone à la fenêtre MainWindow
// TODO créer un splash screen pour le launch4j
// TODO ajouter un bundled jre pour le launch4j


import com.darksidegames.docMaker.DebugManager;
import com.darksidegames.docMaker.Documentation;
import com.darksidegames.docMaker.LexiqueManager;
import com.darksidegames.docMaker.config.GeneralConfigUnit;
import com.darksidegames.docMaker.config.AppearanceConfigUnit;
import com.darksidegames.docMaker.config.ExitCodes;
import com.darksidegames.docMaker.exceptions.MalformedDataException;
import com.darksidegames.docMaker.ui.MainWindow;

import static com.darksidegames.docMaker.util.IOUtils.fread;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.fusesource.jansi.AnsiConsole;


/**
 * Le point d'entrée en mode CLI de l'application.
 * 
 * @author winterskill
 */
public class EntryPoint
{
	protected static String operation = "";
	protected static String buildTarget = "";
	protected static String buildOutputFolder = "";
	protected static boolean isGUIMode;
	
	/**
	 * Représente la documentation actuellement éditée dans l'interface
	 * graphique.
	 */
	public static Documentation actualDocumentation;
	
	/**
	 * Définit si l'application est lancée en mode GUI ou non.
	 * 
	 * @param b
	 * La valeur
	 */
	public static final void setIsGUIMode(final boolean b) {
		isGUIMode = b;
	}
	
	/**
	 * Retourne si l'application est lancée en mode GUI ou non.
	 */
	public static final boolean getIsGUIMode() {
		return isGUIMode;
	}
	
	public static void main(String[] args) throws MalformedDataException
	{
		AnsiConsole.systemInstall();
		
		try {
			GeneralConfigUnit.init();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		try {
			AppearanceConfigUnit.init();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		for (int i = 0; i < args.length; i++) {
			switch (args[i]) {
			case "/d":
				GeneralConfigUnit.debugMode = true;
				DebugManager.initDebug(); // initialisation supplémentaire du DebugManager pour être sûr d'afficher les messages de debug à l'intérieur
				                          // de ce switch.
				DebugManager.print("com.darksidegames.docMaker.entryPoint:EntryPoint@main", "Debug mode enabled");
				break;
			case "/h":
				System.out.println("Ceci est la version alpha de DocMaker");
				System.out.println("DocMaker est un generateur de documentation multilangages.");
				System.out.println("Bien qu'il soit actuellement uniquement disponible en mode console, il disposera\n"
						+ "dans sa version finale d'une interface graphique.");
				System.out.println("Contrairement a bien d'autres generateurs de documentations, docMaker prend ses sources\n"
						+ "depuis des fichiers JSON.");
				System.out.println("");
				System.out.println("Parametres :");
				System.out.println("/d     :     Lance le programme en mode debug");
				System.out.println("/h     :     Affiche cette aide");
				System.out.println("-v     :     Affiche la version du programme");
				System.out.println("--log <filepath>     :     Specifie le fichier dans lequel afficher le log");
				System.out.println("/l     :     Doit-t-on ecrire le log (ne fonctionne qu'avec le mode\n" +
							"             debug");
				System.out.println("--build <file> : Lance le programme pour builder une documentation a partir\n"
						+ "                 de sources JSON");
				System.out.println("--output <dir> : Specifie le dossier ou placer la documentation generee");
				EntryPoint.quitProgram(ExitCodes.PARAM_HELP_GIVEN.getCode());
				break;
			case "-v":
				System.out.println(GeneralConfigUnit.applicationTitle);
				System.out.println("version : " + GeneralConfigUnit.applicationVersion);
				EntryPoint.quitProgram(ExitCodes.PARAM_VERSION_GIVEN.getCode());
				break;
			case "--log":
				if (args.length >= i + 1) {
					GeneralConfigUnit.logFilePath = args[i + 1];
					i++;
					break;
				}
				break;
			case "/l":
				GeneralConfigUnit.writeLog = true;
				break;
			case "--build":
				// si on veut build une doc existante
				if (args.length >= i + 1) {
					operation = "build";
					buildTarget = args[i + 1];
					i++;
					break;
				} else {
					System.out.println("PLEASE GIVE A JSON DATAS FILE");
					EntryPoint.quitProgram(ExitCodes.NO_DOCUMENTATION_DATAS_GIVEN.getCode());
					break;
				}
			case "--output":
				if (args.length >= i + 1) {
					buildOutputFolder = args[i + 1];
					i++;
					break;
				} else {
					buildOutputFolder = "output";
					// Le dossier d'output par défaut.
					break;
				}
			case "--kill":
				// easter egg
				System.out.println("omae wa mo shindeiru");
				EntryPoint.quitProgram(ExitCodes.OMAE_WA_MO_SHINDEIRU.getCode());
				break;
			}
		}
		
		DebugManager.initDebug();
		LexiqueManager.initLexique("fr_FR");
		
		DebugManager.print("com.darksidegames.docMaker.entryPoint:EntryPoint@main", "Starting...");
		
		switch (operation) {
		case "build":
			setIsGUIMode(false);
			if (buildTarget == "") {
				DebugManager.print("com.darksidegames.docMaker.entryPoint:EntryPoint@main", "No JSON datas given!", DebugManager.MESSAGE_ERROR);
				EntryPoint.quitProgram(ExitCodes.NO_DOCUMENTATION_DATAS_GIVEN.getCode());
			}
			if (buildOutputFolder == "") {
				DebugManager.print("com.darksidegames.docMaker.entryPoint:EntryPoint@main", "No output folder given!", DebugManager.MESSAGE_ERROR);
				EntryPoint.quitProgram(ExitCodes.NO_OUTPUT_FOLDER_GIVEN.getCode());
			}
			if (!new File(buildTarget).exists()) {
				DebugManager.print("com.darksidegames.docMaker.entryPoint:EntryPoint@main", "The JSON datas file does not exists !", DebugManager.MESSAGE_ERROR);
				EntryPoint.quitProgram(ExitCodes.DOCUMENTATION_DATAS_FILE_DOES_NOT_EXISTS.getCode());
			}
			try {
				Documentation doc = new Documentation(fread(buildTarget));
				doc.validate();
				doc.calcStatistics();
				try {
					doc.generate(buildOutputFolder);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				EntryPoint.quitProgram(ExitCodes.STANDARD.getCode());
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			break;
		default:
			setIsGUIMode(true);
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (Exception e) {
				DebugManager.print("com.darksidegames.docMaker.entryPoint:EntryPoint@main", "Cannot load Look and Feel", DebugManager.MESSAGE_ERROR);
				JOptionPane.showMessageDialog(null, "Cannot load Look and Feel", "ERROR", JOptionPane.ERROR_MESSAGE);
			}
			
			MainWindow mainWindow = new MainWindow();
		}
	}
	
	public static void quitProgram(int exitCode)
	{
		DebugManager.print("com.darksidegames.docMaker.entryPoint:EntryPoint@quitProgram", "Quit program with code : " + exitCode, DebugManager.MESSAGE_WARNING);
		AnsiConsole.systemUninstall();
		System.exit(exitCode);
		return;
	}
}
