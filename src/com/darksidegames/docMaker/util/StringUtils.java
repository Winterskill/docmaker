/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.util;

/**
 * Cette classe abstraite contient des fonctions utilitaires sur la gestion de
 * fichiers.
 * Il est recommandé de l'utiliser avec des imports statiques.
 * 
 * @author winterskill
 *
 */
public abstract class StringUtils
{
	/**
	 * Retourne si un String est vide ou non.
	 * 
	 * @param s
	 * Le string à tester.
	 */
	public static boolean isStringEmpty(String s)
	{
		return s == null || s.equals("");
	}
}