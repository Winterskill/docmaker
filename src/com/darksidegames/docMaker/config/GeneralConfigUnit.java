/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.config;

import java.io.IOException;
import java.util.Properties;

import com.darksidegames.docMaker.DebugManager;


/**
 * La configuration générale.
 */
public abstract class GeneralConfigUnit {
	/**
	 * Le titre de l'application.
	 */
	public static String applicationTitle = "docMaker";
	/**
	 * La version de l'application.
	 */
	public static String applicationVersion = "1.1.0pre1";
	/**
	 * L'application est-t-elle en mode debug ou non?
	 */
	public static boolean debugMode = false;
	/**
	 * Doit-on écrire un log? (n'écrit qu'en mode debug)
	 */
	public static boolean writeLog = true;
	/**
	 * Doit-t-on vider le fichier de log au démarrage ?
	 */
	public static boolean emptyLogOnStarting = true;
	/**
	 * Le chemin d'accès au fichier de log.
	 */
	public static String logFilePath = "docMaker.log";
	/**
	 * L'objet pour manipuler le fichier de configuration {@code general.cfg}
	 */
	protected static Properties configFile;
	
	public static void init() throws IOException
	{
		// TODO ce n'est qu'une méthode temporaire, en attendant mieux
		DebugManager.print("com.darksidegames.docMaker.config:GeneralConfigUnit@init",
				"GeneralConfigUnit initialization.");
		
		configFile = ConfigUnitUtils.initFile("config/general.cfg");
		
		if (configFile.containsKey("application.title"))
			GeneralConfigUnit.applicationTitle = configFile.getProperty("application.title");

		if (configFile.containsKey("debug.mode"))
			GeneralConfigUnit.debugMode = (configFile.getProperty("debug.mode") == "0") ? false : true;
		if (configFile.containsKey("debug.writeLog"))
			GeneralConfigUnit.writeLog = (configFile.getProperty("debug.writeLog") == "0") ? false : true;
		if (configFile.containsKey("debug.emptyLogOnStarting"))
			GeneralConfigUnit.emptyLogOnStarting = (configFile.getProperty("debug.emptyLogOnStarting") == "0") ? false : true;
		if (configFile.containsKey("debug.logFilePath"))
			GeneralConfigUnit.logFilePath = configFile.getProperty("debug.logFilePath");
	}
}