/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.darksidegames.docMaker.DebugManager;


/**
 * Cette classe contient les <i>utils</i> pour les classes de configuration.
 * 
 * @author winterskill
 */
public abstract class ConfigUnitUtils
{
	/**
	 * Initialise l'unité de configuration. Doit être appelée au début du code,
	 * avant l'utilisation de l'unité de configuration.
	 * 
	 * @param filename
	 * Le nom du fichier INI dans lequel seront stockées les données de
	 * configuration.
	 * 
	 * @throws IOException
	 */
	/*
	public static final Wini initFile(String filename) throws IOException
	{
		DebugManager.print("com.darksidegames.docMaker.config:ConfigUnitUtils@initFile",
				"Init config unit");
		
		File file = new File(filename);
		
		if (!ConfigUnitUtils.configFileExists(filename)) {
			DebugManager.print("com.darksidegames.docMaker.config:ConfigUnitUtils@initFile",
					"file does not exists", DebugManager.MESSAGE_ERROR);
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if (ConfigUnitUtils.isConfigFileDirectory(filename)) {
			DebugManager.print("com.darksidegames.docMaker.config:ConfigUnitUtils@initFile",
					"file is a directory!", DebugManager.MESSAGE_ERROR);
			throw new IOException("the file " + filename + " must be a file and not a "
					+ "directory");
		}
		
		return new Wini(file);
	}*/
	
	public static final Properties initFile(String filename) throws IOException
	{
		DebugManager.print("com.darksidegames.docMaker.config:ConfigUnitUtils@initFile",
				"Init config unit");
		
		File file = new File(filename);
		
		if (!ConfigUnitUtils.configFileExists(filename)) {
			DebugManager.print("com.darksidegames.docMaker.config:ConfigUnitUtils@initFile",
					"file does not exists", DebugManager.MESSAGE_ERROR);
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if (ConfigUnitUtils.isConfigFileDirectory(filename)) {
			DebugManager.print("com.darksidegames.docMaker.config:ConfigUnitUtils@initFile",
					"file is a directory!", DebugManager.MESSAGE_ERROR);
			throw new IOException("the file " + filename + " must be a file and not a "
					+ "directory");
		}
		
		Properties prop = new Properties();
		prop.load(new FileInputStream(file));
		
		return prop;
	}
	
	/**
	 * Retourne si le fichier de configuration existe ou non.
	 * 
	 * @param filename
	 * Le chemin du fichier.
	 */
	public static final boolean configFileExists(String filename)
	{
		File f = new File(filename);
		return f.exists();
	}
	
	/**
	 * Retourne si le fichier de configuration est en fait un dossier.
	 * 
	 * @param filename
	 * Le chemin du fichier.
	 */
	public static final boolean isConfigFileDirectory(String filename)
	{
		File f = new File(filename);
		return f.isDirectory();
	}
}
