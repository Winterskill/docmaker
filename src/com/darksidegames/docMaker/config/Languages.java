/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.config;

import com.darksidegames.docMaker.json.JSONFunctionArgumentModel;
import com.darksidegames.docMaker.json.JSONPackageModel;
import com.darksidegames.docMaker.json.JSONSourceModel;
import com.darksidegames.docMaker.json.JSONTemplateArgumentModel;
import com.google.gson.annotations.SerializedName;
import com.darksidegames.docMaker.json.JSONScope;
import com.darksidegames.docMaker.DebugManager;
import com.darksidegames.docMaker.LexiqueManager;
import com.darksidegames.docMaker.doc.ObjectsFinder;
import com.darksidegames.docMaker.doc.ObjectsFinderObject;
import com.darksidegames.docMaker.doc.ObjectsFinderSearchOrder;
import com.darksidegames.docMaker.exceptions.MalformedDataException;
import com.darksidegames.docMaker.exceptions.NoObjectNameException;
import com.darksidegames.docMaker.exceptions.NotAnArrayException;
import com.darksidegames.docMaker.exceptions.TermNotFoundException;
import static com.darksidegames.docMaker.util.StringUtils.isStringEmpty;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * La liste des langages supportés par le générateur.
 * 
 * @author winterskill
 */
public enum Languages
{
	@SerializedName("calypso")
	CALYPSO("Calypso", "@", ".", ".", "[", "]", JSONScope.PUBLIC) {
		@Override
		public void validate(JSONSourceModel datas) throws MalformedDataException {
			// validation du JSON
			DebugManager.print("com.darksidegames.docMaker.config:Languages.CALYPSO@validate", "Validating...");
			if (datas.packages != null) {
				// si il y a des packages
				for (int packagesCount = 0;
						packagesCount < datas.packages.size();
						packagesCount++) {
					String packagesCountS = new StringBuilder().append("").append(packagesCount).toString();
					
					if (datas.packages.get(packagesCount).suitcases != null) {
						// si il y a des suitcases
						for (int suitcasesCount = 0;
								suitcasesCount < datas.packages.get(packagesCount).suitcases.size();
								suitcasesCount++) {
							String suitcasesCountS = new StringBuilder().append("").append(suitcasesCount).toString();
							
							if (datas.packages.get(packagesCount).suitcases.get(suitcasesCount).name == null)
								throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].suitcases[" + suitcasesCountS +
										"].name : mandatory element");
							
							if (datas.packages.get(packagesCount).suitcases.get(suitcasesCount).content != null) {
								// si il y a du contenu
								for (int contentCount = 0;
										contentCount < datas.packages.get(packagesCount).suitcases.get(suitcasesCount).content.size();
										contentCount++) {
									String contentCountS = new StringBuilder().append("").append(contentCount).toString();
									
									if (datas.packages.get(packagesCount).suitcases.get(suitcasesCount).content.get(contentCount).name == null)
										throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].suitcases[" +
												suitcasesCountS + "].content[" + contentCountS + "].name : mandatory element");
								}
							}
						}
					}
				}
			}
			
			DebugManager.print("com.darksidegames.docMaker.config:Languages.CALYPSO@validate", "documentation validated");
		}
		
		@Override
		public void calcStatistics(JSONSourceModel datas, HashMap<String, Integer> contentTypeDetails)
		{
			if (datas.packages != null) {
				for (int packagesCount = 0;
						packagesCount < datas.packages.size();
						packagesCount++) {
					if (datas.packages.get(packagesCount).suitcases != null) {
						for (int suitcasesCount = 0;
								suitcasesCount < datas.packages.get(packagesCount).suitcases.size();
								suitcasesCount++) {
							if (contentTypeDetails.containsKey("suitcases")) {
								contentTypeDetails.put("suitcases", contentTypeDetails.get("suitcases") + 1);
							} else {
								contentTypeDetails.put("suitcases", 1);
							}
						}
					}
				}
			}
			
			DebugManager.print("com.darksidegames.docMaker.config:Languages.CALYPSO@calcStatistics", "statistics calculated");
		}
		
		@Override
		public boolean objectIsArray(String object)
		{
			boolean retval = false;
			
			Pattern regexPattern = Pattern.compile("^.+\\[\\d*\\]$");
			Matcher regexMatcher = regexPattern.matcher(object);
			
			while (regexMatcher.find())
				retval = true;
			
			return retval;
		}
		
		@Override
		public String arrayGetName(String object) throws NotAnArrayException
		{
			Pattern regexPattern = Pattern.compile("\\[");
			String[] regexMatcher = regexPattern.split(object);
			if (regexMatcher.length > 0)
				return regexMatcher[0];
			else
				throw new NotAnArrayException(object + " is not an array");
		}

		@Override
		public String objectGetName(String objectFullName) throws NoObjectNameException
		{
			String retval = "";
			String endPackagesSeparator = this.getEndPackagesSeparator();
			
			//Pattern regexPattern = Pattern.compile(endPackagesSeparator);
			Pattern regexPattern = Pattern.compile("/\\./");
			String[] regexResults = regexPattern.split(objectFullName);
			if (regexResults.length > 0)
				retval = regexResults[regexResults.length - 1]; // soit la dernière entrée
			else
				throw new NoObjectNameException(objectFullName + " has no name");
			
			// si il s'agit d'un tableau
			if (this.objectIsArray(retval)) try {
				retval = this.arrayGetName(retval);
			} catch (NotAnArrayException e) {
				e.printStackTrace();
			}
			
			return retval;
		}

		@Override
		public String arrayGetSize(String object) throws NotAnArrayException
		{
			String retval = "";
			
			if (this.objectIsArray(object)) {
				Pattern regexPattern = Pattern.compile("[\\[\\]]");
				String[] regexResults = regexPattern.split(object);
				if (regexResults.length >= 2)
					retval = regexResults[1];
				else
					retval = "";
			} else
				throw new NotAnArrayException(object + " is not an array");
			
			return retval;
		}

		@Override
		public String objectGetPackage(String objectFullName)
		{
			String retval = "";
			String endPackagesSeparator = this.getEndPackagesSeparator();
			
			//Pattern regexPattern = Pattern.compile(endPackagesSeparator);
			Pattern regexPattern = Pattern.compile("/\\./");
			String[] regexResults = regexPattern.split(objectFullName);
			
			if (regexResults.length > 0)
				retval = regexResults[0];
			
			return retval;
		}
		
		@Override
		public void fillObjectsFinder(ObjectsFinder o, JSONSourceModel m)
		{
			if (m.packages != null) {
				for (int i = 0; i < m.packages.size(); i++) {
					if (m.packages.get(i).suitcases != null) {
						for (int j = 0; j < m.packages.get(i).suitcases.size(); j++) {
							o.register(m.packages.get(i).suitcases.get(j).name, ObjectsFinderObject.ENUM, m.packages.get(i).name);
						}
					}
				}
			}
			
			DebugManager.print("com.darksidegames.docMaker.config:Languages@fillObjectsFinder",
					"Objects finder filled");
		}
		
		@Override
		public String generateDeprecatedOutputFile(JSONSourceModel m)
		{
			String retval = "";
			
			if (m.packages != null) {
				for (int i = 0; i < m.packages.size(); i++) {
					if (m.packages.get(i).suitcases != null) {
						for (int j = 0; j < m.packages.get(i).suitcases.size(); j++) {
							if (m.packages.get(i).suitcases.get(j).deprecated)
								retval += "<li><a href=\"elements/"
										+ m.packages.get(i).name
										+ this.getEndPackagesSeparator()
										+ m.packages.get(i).suitcases.get(j).name
										+ ".html\">"
										+ m.packages.get(i).name
										+ this.getEndPackagesSeparator()
										+ m.packages.get(i).suitcases.get(j).name
										+ "</a></li>";
						}
					}
				}
			}
			
			DebugManager.print("com.darksidegames.docMaker.config:Languages@generateDeprecatedOutputFile",
					"deprecated.html file generated");
			
			return retval;
		}
		
		@Override
		public String generatePackageOutputFile(JSONPackageModel p, String v)
		{
			String retval = "";
			
			if (p.suitcases != null) {
				HashMap<String, String> lstSts = new HashMap<String, String>();
				int elementsCount = 0;
				for (int i = 0; i < p.suitcases.size(); i++) {
					elementsCount++;
					String val = "";
					if (!isStringEmpty(p.suitcases.get(i).shortDescription))
						val = p.suitcases.get(i).shortDescription;
					else if (!isStringEmpty(p.suitcases.get(i).description)) {
						if (p.suitcases.get(i).description.length() < AppearanceConfigUnit.descriptionMaxLength)
							val = p.suitcases.get(i).description;
						else {
							val = p.suitcases.get(i).description.substring(0, AppearanceConfigUnit.descriptionMaxLength);
							val += "...";
						}
					}
					
					String lblNew = "";
					if (!isStringEmpty(p.suitcases.get(i).since)
							&& !isStringEmpty(v)
							&& p.suitcases.get(i).since.equals(v))
						lblNew = " " + AppearanceConfigUnit.getLabelNew();
					
					lstSts.put("<code><a href=\""
							+ p.name
							+ this.getEndPackagesSeparator()
							+ p.suitcases.get(i).name
							+ ".html\">"
							+ p.name
							+ this.getEndPackagesSeparator()
							+ p.suitcases.get(i).name
							+ "</a></code>", val);
				}
				try {
					String s = new StringBuilder().append("").append(elementsCount).toString();
					retval += AppearanceConfigUnit.generateTable(lstSts,
							LexiqueManager.getTerm("calypso.title.suitcases", "output") + " <span class=\"badge\">" + s + "</span>");
				} catch (TermNotFoundException e) {
					e.printStackTrace();
				}
				retval += "<br/>";
			}
			
			return retval;
		}
		
		@Override
		public String generateFunctionArg(JSONFunctionArgumentModel m, ObjectsFinder o, String actualPackage)
		{
			String retval = "<code>";
			int keyModifiersCount = 0;
			
			try {
				retval += o.renderObject(m.type, ObjectsFinderSearchOrder.TYPE, false, actualPackage);
			} catch (NoObjectNameException e) {
				e.printStackTrace();
				retval += m.type;
			}
			retval += " ";
			
			if (m.isConst) {
				if (keyModifiersCount == 0)
					retval += "const";
				else
					retval += " const";
				keyModifiersCount++;
			}
			
			if (m.ref) {
				if (keyModifiersCount == 0)
					retval += "&";
				else
					retval += " &";
				keyModifiersCount++;
			}
			
			if (m.pointer) {
				if (keyModifiersCount == 0)
					retval += "*";
				else
					retval += " *";
				keyModifiersCount++;
			}
			
			if (keyModifiersCount == 0)
				retval += m.name;
			else
				retval += " " + m.name;
			
			if (!isStringEmpty(m.defaultValue)) {
				if (keyModifiersCount == 0)
					retval += "= " + m.defaultValue;
				else
					retval += " = " + m.defaultValue;
				keyModifiersCount++;
			}
			
			retval += "</code>";
			
			return retval;
		}
		
		@Override
		public String generateFunctionTemplateArg(JSONTemplateArgumentModel m, ObjectsFinder o)
		{
			String retval = "<code>";
			retval += m.name;
			
			if (!isStringEmpty(m.defaultValue))
				retval += " = " + m.defaultValue;
			
			retval += "</code>";
			
			return retval;
		}
	};
	
	/**
	 * Valide le document source selon les critères propres au langage.
	 * 
	 * @param datas
	 * Les données.
	 * @throws MalformedDataException 
	 */
	public abstract void validate(JSONSourceModel datas) throws MalformedDataException;
	/**
	 * Calcule le nombre d'éléments présents dans la documentation.
	 * 
	 * @param datas
	 * Les données.
	 * @param contentTypeDetails
	 * Le tableau dans lequel mettre les résultats.
	 */
	public abstract void calcStatistics(JSONSourceModel datas, HashMap<String, Integer> contentTypeDetails);
	/**
	 * Retourne si l'objet donné est un tableau ou non.
	 * 
	 * @param object
	 * Le nom de l'objet. Par exemple <code>monTab[44]</code>.
	 */
	public abstract boolean objectIsArray(String object);
	/**
	 * Retourne le nom du tableau entré en paramètres.
	 * 
	 * Par exemple, si on fait :
	 * <pre>String s = arrayGetName("tab.leau[12]");</pre>
	 * <code>s</code> vaudra <code>"tab.leau"</code>.
	 * 
	 * @param object
	 * Le tableau.
	 * @throws NotAnArrayException
	 */
	public abstract String arrayGetName(String object) throws NotAnArrayException;
	/**
	 * Extrait le nom d'un objet s'il est contenu dans un nom de package.
	 * 
	 * Par exemple, si on fait :
	 * <pre>String s = objectGetName("com.package.name.Object");</pre>
	 * <code>s</code> vaudra <code>"Object"</code>.
	 * </p>
	 * Fonctionne également avec les tableaux :
	 * <pre>String s = objectGetName("com.package.name.Object[12]");</pre>
	 * <code>s</code> vaudra <code>"Object"</code> quand même.
	 * @param objectFullName
	 * Le nom de l'objet.
	 * @throws NoObjectNameException 
	 */
	public abstract String objectGetName(String objectFullName) throws NoObjectNameException;
	/**
	 * Extrait la taille d'un tableau. Si le tableau n'a pas de taille précisée,
	 * retourne une chaine vide.
	 * 
	 * @param object
	 * Le tableau.
	 * @throws NotAnArrayException
	 */
	public abstract String arrayGetSize(String object) throws NotAnArrayException;
	/**
	 * Extrait le nom du package d'un objet.
	 * 
	 * @param objectFullName
	 * L'objet.
	 * @return
	 * Le nom du package. Si l'objet n'a pas de package, retourne une chaine vide.
	 */
	public abstract String objectGetPackage(String objectFullName);
	/**
	 * Remplit le chercheur d'objets selon les spécificités du langage de programmation.
	 * 
	 * @param objectsFinder
	 * Le chercheur d'objets.
	 * @param datas
	 * Les données fournies.
	 */
	public abstract void fillObjectsFinder(ObjectsFinder objectsFinder, JSONSourceModel datas);
	/**
	 * Scanne selon les données spécifiques au langage de programmation pour en
	 * extraire les données obsolètes, et les inscrit sur le fichier
	 * <code>deprecated.html</code>.
	 * 
	 * @param datas
	 * Les données
	 * @return Le code HTML à inscrire dans le fichier.
	 */
	public abstract String generateDeprecatedOutputFile(JSONSourceModel datas);
	/**
	 * Scanne les données spécifiées par <code>datas</code> pour en extraire
	 * une liste HTML avec les éléments du langage du package.
	 * 
	 * @param datas
	 * Les données du package.
	 * @param docVersion
	 * La version de la documentation.
	 * @return Le code HTML à inscrire dans le fichier.
	 */
	public abstract String generatePackageOutputFile(JSONPackageModel datas, String docVersion);
	/**
	 * Scanne les données spécifiées par <code>arg</code> pour générer un
	 * <code>String</code> qui contient l'argument sous sa forme dans le code
	 * source.
	 * 
	 * @param arg
	 * Les données de l'argument.
	 * @param o
	 * Le chercheur d'objet de la documentation dans laquelle cette méthode est
	 * utilisée.
	 * @param actualPackage
	 * Le nom du package actuel
	 * 
	 * @return La forme du code source de l'argument.
	 */
	public abstract String generateFunctionArg(JSONFunctionArgumentModel arg, ObjectsFinder o, String actualPackage);
	/**
	 * Scanne les données spécificités par <code>arg</code> pour générer un
	 * <code>String</code> qui contient l'argument de template sous sa forme
	 * dans le code source.
	 * 
	 * @param arg
	 * Les données de l'argument de template.
	 * @param o
	 * Le chercheur d'objet de la documentation dans laquelle cette méthode est
	 * utilisée.
	 * 
	 * @return La forme "code source" de l'argument de template.
	 */
	public abstract String generateFunctionTemplateArg(JSONTemplateArgumentModel arg, ObjectsFinder o);
	// TODO ajouter la méthode generate(JSONSourceModel datas, String path)
	
	/**
	 * Le nom affiché.
	 */
	private String displayedName;
	/**
	 * Le ou les caractère(s) utilisé(s) comme préfixe(s) pour les annotations.
	 */
	private String annotationsPrefix;
	/**
	 * Le ou les caractère(s) utilisé(s) comme séparateur entre les différentes
	 * parties d'un nom de package. Par exemple, en Java, c'est "{@code .}", et
	 * en PHP "{@code /}".
	 */
	private String packagesSeparator;
	/**
	 * Le ou les caractère(s) utilisé(s) comme ouverture de signalisation de
	 * déclaration de tableau dans la déclaration d'une variable. Dans la
	 * plupart des langages, il s'agit de "{@code [}".
	 */
	private String arrayDeclarationOpenChar;
	/**
	 * Le ou les caractère(s) utilisé(s) comme fermeture de signalisation de
	 * déclaration de tableau dans la déclaration d'une variable. Dans la
	 * plupart des langages, il s'agit de "{@code ]}".
	 */
	private String arrayDeclarationCloseChar;
	/**
	 * La portée par défaut des éléments du langages.
	 * 
	 * Utilisé quand un élément n'a pas de portée explicitement déclarée dans
	 * le fichier JSON.
	 */
	private JSONScope defaultScope;
	/**
	 * Ce qui sépare la fin du package et le nom de l'objet.
	 */
	private String endPackagesSeparator;
	
	private Languages(String displayedName,
			String annotationsPrefix, String packagesSeparator,
			String endPackagesSeparator, String arrayDeclarationOpenChar,
			String arrayDeclarationCloseChar, JSONScope defaultScope)
	{
		this.displayedName = displayedName;
		this.annotationsPrefix = annotationsPrefix;
		this.packagesSeparator = packagesSeparator;
		this.endPackagesSeparator = endPackagesSeparator;
		this.arrayDeclarationOpenChar = arrayDeclarationOpenChar;
		this.arrayDeclarationCloseChar = arrayDeclarationCloseChar;
		this.defaultScope = defaultScope;
	}
	
	/**
	 * Retourne le nom affiché.
	 */
	public String getDisplayedName()
	{
		return this.displayedName;
	}
	
	/**
	 * Retourne le préfixe des annotations.
	 */
	public String getAnnotationsPrefix()
	{
		return this.annotationsPrefix;
	}
	
	/**
	 * Retourne le caractère utilisé comme séparateur dans les noms de packages.
	 */
	public String getPackagesSeparator()
	{
		return this.packagesSeparator;
	}
	
	/**
	 * Retourne le caractère utilisé comme séparateur entre la fin du nom du 
	 * package et le nom de l'objet.
	 */
	public String getEndPackagesSeparator()
	{
		return this.endPackagesSeparator;
	}
	
	/**
	 * <p>Retourne le caractère ou la suite de caractères utilisée pour débuter
	 * la signalisation d'un tableau dans une déclaration de variable.</p>
	 * <p>Dans beaucoup de langages, il s'agit de <code>[</code>.</p>
	 */
	public String getArrayDeclarationOpenChar()
	{
		return this.arrayDeclarationOpenChar;
	}
	
	/**
	 * <p>Retourne le caractère ou la suite de caractères utilisée pour terminer
	 * la signalisation d'un tableau dans une déclaration de variable.</p>
	 * <p>Dans beaucoup de langages, il s'agit de <code>]</code>.</p>
	 */
	public String getArrayDeclarationCloseChar()
	{
		return this.arrayDeclarationCloseChar;
	}
	
	/**
	 * <p>Retourne la portée par défaut du langage.</p>
	 */
	public JSONScope getDefaultScope()
	{
		return this.defaultScope;
	}
}
