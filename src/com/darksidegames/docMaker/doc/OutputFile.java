/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.doc;

import com.darksidegames.docMaker.DebugManager;
import static com.darksidegames.docMaker.util.IOUtils.fwrite;

/**
 * Cette classe représente un fichier faisant partie de la documentation générée.
 * L'utilisation de cette classe permet de pouvoir insérer du contenu automatique
 * dans le fichier, comme par exemple un commentaire au début.
 * 
 * @author winterskill
 */
public class OutputFile
{
	public OutputFile(String filepath, String content)
	{
		DebugManager.print("com.darksidegames.docMaker.doc:OutputFile@OutputFile", "Creating file " + filepath);
		fwrite(filepath, content, false);
	}
}
