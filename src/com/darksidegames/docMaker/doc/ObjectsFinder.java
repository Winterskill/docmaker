/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.doc;

import java.util.ArrayList;
import java.util.HashMap;

import com.darksidegames.docMaker.DebugManager;
import com.darksidegames.docMaker.config.Languages;
import com.darksidegames.docMaker.exceptions.NoObjectNameException;
import com.darksidegames.docMaker.exceptions.NotAnArrayException;


/**
 * Cette classe permet de savoir si tel ou tel objet est présent dans
 * la documentation générée, afin de permettre de faire des liens
 * automatiquement entre différents objets (par exemple, une classe MaClass1,
 * qui possède une méthode qui retourne une instance de MaClass2 : si MaClass2
 * existe dans la documentation, l'utilisateur pourra cliquer sur le lien vers
 * MaClass2 dans la fiche sur MaClass1).
 * 
 * @author winterskill
 */
public class ObjectsFinder
{
	/**
	 * La liste de tout ce qui est documenté, sous la forme
	 * documented.get(TYPE).add(ELEMENT);
	 */
	protected HashMap<ObjectsFinderObject, ArrayList<String>> documented;
	protected Languages language;
	protected ArrayList<String> langPackages;
	
	/**
	 * Crée un nouveau chercheur d'objets.
	 * 
	 * @param lang
	 * Le langage de prog utilisé par la documentation.
	 */
	public ObjectsFinder(Languages lang)
	{
		DebugManager.print("com.darksidegames.docMaker.doc:ObjectsFinder@staticInit",
				"Creating new ObjectsFinder");
			
		documented = new HashMap<ObjectsFinderObject, ArrayList<String>>();
		
		this.documented.put(ObjectsFinderObject.ANNOTATION,             new ArrayList<String>());
		this.documented.put(ObjectsFinderObject.CLASS,                  new ArrayList<String>());
		this.documented.put(ObjectsFinderObject.ENUM,                   new ArrayList<String>());
		this.documented.put(ObjectsFinderObject.FUNCTION,               new ArrayList<String>());
		this.documented.put(ObjectsFinderObject.INTERFACE,              new ArrayList<String>());
		this.documented.put(ObjectsFinderObject.PACKAGE,                new ArrayList<String>());
		this.documented.put(ObjectsFinderObject.PREPROCESSOR_CONSTANTS, new ArrayList<String>());
		this.documented.put(ObjectsFinderObject.TYPEDEF,                new ArrayList<String>());
		this.documented.put(ObjectsFinderObject.VARIABLE,               new ArrayList<String>());
		
		language = lang;
		langPackages = new ArrayList<String>();
	}

	/**
	 * Enregistre un package importé par défaut.
	 *
	 * Lors d'une recherche d'un objet, si celui n'existe pas, le finder va
	 * chercher dans le package actuel, puis dans les packages importés
	 * par défaut.
	 * 
	 * @param name
	 * Le nom du package à importer.
	 *
	 * @return void
	 */
	public void registerLangPackage(String name)
	{
		if (!langPackages.contains(name))
			langPackages.add(name);
	}

	/**
	 * Vide la liste des packages importés par défaut.
	 */
	public void emptyLangPackages()
	{
		langPackages = new ArrayList<String>();
	}

	/**
	 * @param name
	 * Le nom du package.
	 */
	public boolean langPackageExists(String name)
	{
		return langPackages.contains(name);
	}
	
	/**
	 * Enregistre un objet dans le chercheur d'objets.
	 * 
	 * @param name
	 * Le nom de l'objet.
	 * @param type
	 * Le type de l'objet.
	 * @param packageName
	 * Le nom du package. Mettez "" pour signifier aucun package.
	 * 
	 * @throws IllegalStateException
	 */
	public void register(String name, ObjectsFinderObject type, String packageName) throws IllegalStateException
	{
		if (name.equals(""))
			throw new IllegalStateException("Argument #1 cannot be empty");
		if (type.equals(""))
			throw new IllegalStateException("Argument #2 cannot be empty");
		
		String fullName = packageName;
		if (!packageName.equals(""))
			fullName += this.language.getEndPackagesSeparator();
		fullName += name;
		
		this.documented.get(type).add(fullName);
	}
	
	/**
	 * Retourne si un objet existe dans la documentation ou non.
	 * 
	 * @param packageName
	 * Le nom du package de l'objet. Mettez "" pour signifier "pas de package".
	 * @param name
	 * Le nom de l'objet.
	 * @param searchOrder
	 * L'ordre de recherche.
	 * @param actualPackage
	 * Le nom du package actuel.
	 * @param searchInActualPackage
	 * Doit-on chercher dans le package actuel si l'objet n'est pas trouvé ?
	 * @param searchInLangPackages
	 * Doit-on chercher dans les packages importés par défaut si l'objet n'est
	 * pas trouvé ?
	 * 
	 * @throws IllegalStateException
	 * Si name est vide.
	 */
	public boolean exists(String packageName, String name, ObjectsFinderSearchOrder searchOrder, String actualPackage, boolean searchInActualPackage, boolean searchInLangPackages) throws IllegalStateException
	{
		if (name.equals(""))
			throw new IllegalStateException("Argument #2 cannot be empty");
		
		String fullName = packageName;
		if (!packageName.equals(""))
			fullName += this.language.getEndPackagesSeparator();
		fullName += name;
		boolean retval = false;
		
		switch (searchOrder)
		{
		case STD:
			if (
					(this.documented.containsKey(ObjectsFinderObject.CLASS)
							&& this.documented.get(ObjectsFinderObject.CLASS).contains(fullName))
					|| (this.documented.containsKey(ObjectsFinderObject.ENUM)
							&& this.documented.get(ObjectsFinderObject.ENUM).contains(fullName))
					|| (this.documented.containsKey(ObjectsFinderObject.INTERFACE)
							&& this.documented.get(ObjectsFinderObject.INTERFACE).contains(fullName))
					|| (this.documented.containsKey(ObjectsFinderObject.TYPEDEF)
							&& this.documented.get(ObjectsFinderObject.TYPEDEF).contains(fullName))
					|| (this.documented.containsKey(ObjectsFinderObject.PREPROCESSOR_CONSTANTS)
							&& this.documented.get(ObjectsFinderObject.PREPROCESSOR_CONSTANTS).contains(fullName)))
				retval = true;
			break;
		case THROW:
			if (
					(this.documented.containsKey(ObjectsFinderObject.CLASS)
							&& this.documented.get(ObjectsFinderObject.CLASS).contains(fullName))
					|| (this.documented.containsKey(ObjectsFinderObject.TYPEDEF)
							&& this.documented.get(ObjectsFinderObject.TYPEDEF).contains(fullName))
					|| (this.documented.containsKey(ObjectsFinderObject.PREPROCESSOR_CONSTANTS)
							&& this.documented.get(ObjectsFinderObject.PREPROCESSOR_CONSTANTS).contains(fullName)))
				retval = true;
			break;
		case TYPE:
			if (
					(this.documented.containsKey(ObjectsFinderObject.CLASS)
							&& this.documented.get(ObjectsFinderObject.CLASS).contains(fullName))
					|| (this.documented.containsKey(ObjectsFinderObject.ENUM)
							&& this.documented.get(ObjectsFinderObject.ENUM).contains(fullName))
					|| (this.documented.containsKey(ObjectsFinderObject.INTERFACE)
							&& this.documented.get(ObjectsFinderObject.INTERFACE).contains(fullName))
					|| (this.documented.containsKey(ObjectsFinderObject.TYPEDEF)
							&& this.documented.get(ObjectsFinderObject.TYPEDEF).contains(fullName))
					|| (this.documented.containsKey(ObjectsFinderObject.PREPROCESSOR_CONSTANTS)
							&& this.documented.get(ObjectsFinderObject.PREPROCESSOR_CONSTANTS).contains(fullName)))
				retval = true;
			break;
		case EXTEND:
			if (
					(this.documented.containsKey(ObjectsFinderObject.CLASS)
							&& this.documented.get(ObjectsFinderObject.CLASS).contains(fullName))
					|| (this.documented.containsKey(ObjectsFinderObject.INTERFACE)
							&& this.documented.get(ObjectsFinderObject.INTERFACE).contains(fullName))
					|| (this.documented.containsKey(ObjectsFinderObject.TYPEDEF)
							&& this.documented.get(ObjectsFinderObject.TYPEDEF).contains(fullName)))
				retval = true;
			break;
		case IMPLEMENTATION:
			if (
					(this.documented.containsKey(ObjectsFinderObject.INTERFACE)
							&& this.documented.get(ObjectsFinderObject.INTERFACE).contains(fullName))
					|| (this.documented.containsKey(ObjectsFinderObject.TYPEDEF)
							&& this.documented.get(ObjectsFinderObject.TYPEDEF).contains(fullName))
					|| (this.documented.containsKey(ObjectsFinderObject.PREPROCESSOR_CONSTANTS)
							&& this.documented.get(ObjectsFinderObject.PREPROCESSOR_CONSTANTS).contains(fullName)))
				retval = true;
			break;
		case ANNOTATION:
			if (
					(this.documented.containsKey(ObjectsFinderObject.ANNOTATION)
							&& this.documented.get(ObjectsFinderObject.ANNOTATION).contains(fullName)))
				retval = true;
			break;
		}

		// TODO vérifier les boucles infinies
		if (!retval && searchInActualPackage)
			retval = this.exists(actualPackage, name, searchOrder, actualPackage, false, false);

		if (!retval && searchInLangPackages) {
			for (int i = 0; i < langPackages.size(); i++) {
				retval = this.exists(this.langPackages.get(i), name, searchOrder, this.langPackages.get(i), false, false);
				if (retval)
					break;
			}
		}
		
		return retval;
	}

	/**
	 * Retourne si un objet existe dans la documentation ou non.
	 * 
	 * @param packageName
	 * Le nom du package de l'objet. Mettez "" pour signifier "pas de package".
	 * @param name
	 * Le nom de l'objet.
	 * @param searchOrder
	 * L'ordre de recherche.
	 * @param actualPackage
	 * Le nom du package actuel.
	 * 
	 * @throws IllegalStateException
	 * Si name est vide.
	 */
	public boolean exists(String packageName, String name, ObjectsFinderSearchOrder searchOrder, String actualPackage) throws IllegalStateException
	{
		return this.exists(packageName, name, searchOrder, actualPackage, true, true);
	}
	
	/**
	 * Retourne le nombre d'entrées dans la documentations selon un type.
	 * 
	 * @param type
	 * le type.
	 */
	public int getRegisteredCount(String type)
	{
		if (this.documented.containsKey(type))
			return this.documented.get(type).size();
		else
			return 0;
	}

	/**
	 * Détermine le chemin d'un objet enregistré.
	 *
	 * Grâce à cette méthode, vous pouvez déterminer le chemin d'un objet qui
	 * est dans le package actuel, ou dans un des packages importés par
	 * défaut, auquel cas il détermine de quel package importé par défaut il
	 * s'agit.
	 * Si le chemin n'a pas été trouvé, retourne <code>"NO_PACKAGE"</code>.
	 *
	 * @param packageName
	 * Le nom du package de l'objet.
	 * @param name
	 * Le nom de l'objet.
	 * @param searchOrder
	 * L'ordre de recherche.
	 * @param actualPackage
	 * Le nom du package actuel.
	 * @param searchInActualPackage
	 * Doit-on chercher dans le package actuel ?
	 * @param searchInLangPackages
	 * Doit-on chercher dans les packages importés par défaut ?
	 *
	 * @return String
	 */
	public String getObjectPackage(String packageName, String name, ObjectsFinderSearchOrder searchOrder, String actualPackage, boolean searchInActualPackage, boolean searchInLangPackages)
	{
		if (this.exists(packageName, name, searchOrder, actualPackage, searchInActualPackage, searchInLangPackages)) {

			if (this.exists(packageName, name, searchOrder, packageName, false, false)) {
				// s'il existe dans le package donné
				return packageName;
			} else {
				if (this.exists(actualPackage, name, searchOrder, actualPackage, false, false)) {
					// s'il existe dans le package actuel
					return actualPackage;
				} else {
					for (int i = 0; i < langPackages.size(); i++) {
						if (this.exists(langPackages.get(i), name, searchOrder, "", false, false)) {
							return langPackages.get(i);
						}
					}
				}
			}
		}
		
		return "NO_PACKAGE";
	}
	
	/**
	 * Retourne le code HTML utilisé pour afficher un objet.
	 * Si l'objet est renseigné dans la documentation, il sera affiché dans un lien.
	 * 
	 * @param objectFullName
	 * Le nom de l'objet, avec son package.
	 * @param searchOrder
	 * L'ordre de recherche.
	 * @param usingCodeMarkup
	 * Doit-on entourer le String retourné par des balises code?
	 * @param actualPackage
	 * Le package actuel.
	 * @param searchInActualPackage
	 * Doit-on chercher dans le package actuel si l'objet n'est pas trouvé ?
	 * @param searchInLangPackages
	 * Doit-on chercher dans les packages importés par défaut si l'objet n'est
	 * pas trouvé ?
	 * @return
	 * le code HTML.
	 * @throws NoObjectNameException Quand <code>objectFullName</code> n'a pas de nom.
	 */
	public String renderObject(String objectFullName, ObjectsFinderSearchOrder searchOrder, boolean usingCodeMarkup, String actualPackage, boolean searchInActualPackage, boolean searchInLangPackages) throws NoObjectNameException
	{
		String retval = (usingCodeMarkup) ? "<code>" : "";
		boolean isArray = this.language.objectIsArray(objectFullName);
		String objName = this.language.objectGetName(objectFullName);
		String objSize = "";
		if (isArray) {
			try {
				objSize = this.language.arrayGetSize(objectFullName);
			} catch (NotAnArrayException e) {
				e.printStackTrace();
			}
		}
		String objDisplayedName = "";
		//String packageName = this.language.objectGetPackage(objectFullName);
		String packageName = this.getObjectPackage(this.language.objectGetPackage(objectFullName), objName, searchOrder, actualPackage, searchInActualPackage, searchInLangPackages);
		
		if (searchOrder == ObjectsFinderSearchOrder.ANNOTATION)
			objDisplayedName += this.language.getAnnotationsPrefix();
		objDisplayedName += objName;
		
		if (this.exists(packageName, objName, searchOrder, actualPackage, searchInActualPackage, searchInLangPackages)) {
			retval += "<a href=\"" + packageName + this.language.getEndPackagesSeparator() + objName + ".html\">";
			retval += objDisplayedName;
			retval += "</a>";
			if (isArray)
				retval += this.language.getArrayDeclarationOpenChar() + objSize
						+ this.language.getArrayDeclarationCloseChar();
		} else {
			retval += objDisplayedName;
			if (isArray)
				retval += this.language.getArrayDeclarationOpenChar() + objSize
						+ this.language.getArrayDeclarationCloseChar();
		}
		
		retval += (usingCodeMarkup) ? "</code>" : "";
		return retval;
	}

	/**
	 * Retourne le code HTML utilisé pour afficher un objet.
	 * Si l'objet est renseigné dans la documentation, il sera affiché dans un lien.
	 * 
	 * @param objectFullName
	 * Le nom de l'objet, avec son package.
	 * @param searchOrder
	 * L'ordre de recherche.
	 * @param usingCodeMarkup
	 * Doit-on entourer le String retourné par des balises code?
	 * @param actualPackage
	 * Le package actuel.
	 * @return
	 * le code HTML.
	 * @throws NoObjectNameException Quand <code>objectFullName</code> n'a pas de nom.
	 */
	public String renderObject(String objectFullName, ObjectsFinderSearchOrder searchOrder, boolean usingCodeMarkup, String actualPackage) throws NoObjectNameException
	{
		return this.renderObject(objectFullName, searchOrder, usingCodeMarkup, actualPackage, true, true);
	}
}
