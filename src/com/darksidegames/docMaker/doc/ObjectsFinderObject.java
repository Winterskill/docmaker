package com.darksidegames.docMaker.doc;

/**
 * Contient les différents objets pris en charge par {@link ObjectsFinder}.
 * 
 * @author winterskill
 */
public enum ObjectsFinderObject {
	ANNOTATION,
	CLASS,
	ENUM,
	FUNCTION,
	INTERFACE,
	PACKAGE,
	PREPROCESSOR_CONSTANTS,
	TYPEDEF,
	VARIABLE;
}
