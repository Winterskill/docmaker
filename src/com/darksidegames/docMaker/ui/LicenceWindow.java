/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.ui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.io.File;

import com.darksidegames.docMaker.DebugManager;
import com.darksidegames.docMaker.config.Licence;

import static com.darksidegames.docMaker.util.IOUtils.fwrite;


/**
 * La fenêtre pour afficher la licence.
 * 
 * @author winterskill
 */
public class LicenceWindow extends JFrame
{
	// TODO change the text of the components to display localized text
	protected JButton button_ok = new JButton("OK");
	protected JButton button_save = new JButton("Sauvegarder");
	protected JTextArea licence_area = new JTextArea();
	
	protected JPanel panel = new JPanel();
	protected JPanel buttons_panel = new JPanel();
	protected BorderLayout bl = new BorderLayout();
	protected GridLayout buttons_layout = new GridLayout(1, 2);

	public LicenceWindow()
	{
		DebugManager.print("com.darksidegames.docMaker.ui:LicenceWindow@LicenceWindow", "Showing new licence window");
		setTitle("Licence");
		setSize(500, 600);
		setLocationRelativeTo(null);

		licence_area.setText(Licence.get());

		button_ok.addActionListener(new button_okListener());
		button_save.addActionListener(new button_saveListener());
		
		panel.setLayout(bl);
		panel.add(new JScrollPane(licence_area), BorderLayout.CENTER);
		buttons_panel.setLayout(buttons_layout);
		buttons_panel.add(button_save);
		buttons_panel.add(button_ok);
		panel.add(buttons_panel, BorderLayout.SOUTH);
		getContentPane().add(panel);
		
		setVisible(true);
	}

	class button_okListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			DebugManager.print("com.darksidegames.docMaker.ui:LicenceWindow:button_okListener@actionPerformed", "Closing licence window");
			LicenceWindow.this.setVisible(false);
			LicenceWindow.this.dispose();
			// TODO si ca marche...
		}
	}

	class button_saveListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			DebugManager.print("com.darksidegames.docMaker.ui:LicenceWindow:button_saveListener@actionPerformed", "Save licence in a file");
			JFileChooser jfc = new JFileChooser();
			jfc.setCurrentDirectory(new File("."));
			int jfc_retval = jfc.showOpenDialog(null);

			File file = null;
			if (jfc_retval == JFileChooser.APPROVE_OPTION) {
				DebugManager.print("com.darksidegames.docMaker.ui:LicenceWindow:button_saveListener@actionPerformed", "OK");
				file = jfc.getSelectedFile();
			} else if (jfc_retval == JFileChooser.CANCEL_OPTION) {
				DebugManager.print("com.darksidegames.docMaker.ui:LicenceWindow:button_saveListener@actionPerformed", "Cancel");
				return;
			} else {
				DebugManager.sendErrorMessage("ERROR", "Invalid file", true);
				return;
			}

			if (file.exists()) {
				DebugManager.print("com.darksidegames.docMaker.ui:LicenceWindow:button_saveListener@actionPerformed", "Selected file already exists. It will be overwritten", DebugManager.MESSAGE_WARNING);
			}
			if (file.isDirectory()) {
				DebugManager.sendErrorMessage("IO Error", "Please select a file and not a directory", true);
				return;
			}

			fwrite(file.getPath(), LicenceWindow.this.licence_area.getText(), false);

			return;
		}
	}
}
