/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker;

import static org.fusesource.jansi.Ansi.ansi;
import static org.fusesource.jansi.Ansi.Color.CYAN;
import static org.fusesource.jansi.Ansi.Color.DEFAULT;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;

import org.fusesource.jansi.Ansi;

import com.darksidegames.docMaker.config.GeneralConfigUnit;
import com.darksidegames.docMaker.entryPoint.EntryPoint;


/**
 * Cette classe gère les messages de debug.
 * 
 * @author winterskill
 */
public abstract class DebugManager
{
	/**
	 * Un message normal.
	 */
	public static final Ansi.Color MESSAGE_STANDARD = Ansi.Color.DEFAULT;
	/**
	 * Un message de warning.
	 */
	public static final Ansi.Color MESSAGE_WARNING = Ansi.Color.YELLOW;
	/**
	 * Un message d'erreur.
	 */
	public static final Ansi.Color MESSAGE_ERROR = Ansi.Color.RED;
	/**
	 * Un message de succès.
	 */
	public static final Ansi.Color MESSAGE_SUCCESS = Ansi.Color.GREEN;
	/**
	 * Permet de n'initialiser le debug manager qu'une seule fois.
	 */
	public static boolean alreadyInitialized = false;
	
	/**
	 * Affiche un message de debug.
	 * 
	 * @param printerPath
	 * Le chemin de la méthode qui affiche le debug, sous le format PACKAGE:CLASS@METHOD, ou PACKAGE:ENUM.CLEF@METHOD
	 * @param message
	 * Le message à afficher
	 * @param messageType
	 * Le type du message.
	 */
	public static void print(String printerPath, String message, Ansi.Color messageType)
	{
		if (GeneralConfigUnit.debugMode) {
			// si le debug est activé
			Date date = new Date();
			DateFormat df = new SimpleDateFormat("hh:mm:ss dd/MM/yyyy");
			String sdf = df.format(date);
			if (messageType == DebugManager.MESSAGE_WARNING)
				System.out.println(ansi().a("[" + sdf + "]").a("[" + GeneralConfigUnit.applicationTitle + " " + GeneralConfigUnit.applicationVersion + "][").fg(CYAN)
						.a(printerPath).fg(DEFAULT).a("]").fg(DebugManager.MESSAGE_WARNING).a("<WARNING> " + message)
						.fg(DEFAULT).reset());
			else if (messageType == DebugManager.MESSAGE_ERROR)
				System.out.println(ansi().a("[" + sdf + "]").a("[" + GeneralConfigUnit.applicationTitle + " " + GeneralConfigUnit.applicationVersion + "][").fg(CYAN)
						.a(printerPath).fg(DEFAULT).a("]").fg(DebugManager.MESSAGE_ERROR).a("<ERROR> " + message)
						.fg(DEFAULT).reset());
			else if (messageType == DebugManager.MESSAGE_SUCCESS)
				System.out.println(ansi().a("[" + sdf + "]").a("[" + GeneralConfigUnit.applicationTitle + " " + GeneralConfigUnit.applicationVersion + "][").fg(CYAN)
						.a(printerPath).fg(DEFAULT).a("]").fg(DebugManager.MESSAGE_SUCCESS).a("<SUCCESS> " + message)
						.fg(DEFAULT).reset());
			else
				System.out.println(ansi().a("[" + sdf + "]").a("[" + GeneralConfigUnit.applicationTitle + " " + GeneralConfigUnit.applicationVersion + "][").fg(CYAN)
						.a(printerPath).fg(DEFAULT).a("] ").fg(messageType).a(message).fg(DEFAULT).reset());
			
			if (GeneralConfigUnit.writeLog) {
				// si il faut écrire un log file
				File logFile = new File(GeneralConfigUnit.logFilePath);
				
				InputStream is = null;
				
				try {
					is = new FileInputStream(logFile);
					byte[] buffer = new byte[(int) logFile.length()];
					is.read(buffer);
					
					String actualContent = new String(buffer, "UTF-8"); // conversion de byte[] en String
					
					try {
						PrintWriter pw = new PrintWriter(GeneralConfigUnit.logFilePath);
						pw.print(actualContent);
						if (messageType == DebugManager.MESSAGE_ERROR)
							pw.println("[" + sdf + "][" + GeneralConfigUnit.applicationTitle + " " + GeneralConfigUnit.applicationVersion + "][" + printerPath + "]<ERROR> " + message);
						else if (messageType == DebugManager.MESSAGE_SUCCESS)
							pw.println("[" + sdf + "][" + GeneralConfigUnit.applicationTitle + " " + GeneralConfigUnit.applicationVersion + "][" + printerPath + "]<SUCCESS> " + message);
						else if (messageType == DebugManager.MESSAGE_WARNING)
							pw.println("[" + sdf + "][" + GeneralConfigUnit.applicationTitle + " " + GeneralConfigUnit.applicationVersion + "][" + printerPath + "]<WARNING> " + message);
						else
							pw.println("[" + sdf + "][" + GeneralConfigUnit.applicationTitle + " " + GeneralConfigUnit.applicationVersion + "][" + printerPath + "] " + message);
						pw.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Affiche un message de debug.
	 * 
	 * @param printerPath
	 * Le chemin de la méthode qui affiche le debug, sous le format PACKAGE:CLASS@METHOD
	 * @param message
	 * Le message à afficher
	 */
	public static void print(String printerPath, String message)
	{
		DebugManager.print(printerPath, message, DebugManager.MESSAGE_STANDARD);
	}
	
	/**
	 * Initialise les options de debug.
	 * A effectuer en début de programme.
	 */
	public static void initDebug()
	{
		if (!alreadyInitialized) {
			alreadyInitialized = true;
			
			if (GeneralConfigUnit.debugMode && GeneralConfigUnit.writeLog) {
				File logFile = new File(GeneralConfigUnit.logFilePath);

				if (GeneralConfigUnit.emptyLogOnStarting) {
					if (logFile.exists()) {
						try {
							PrintWriter pw = new PrintWriter(GeneralConfigUnit.logFilePath);
							pw.print("");
							pw.close();
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}
					}
				}

				if (!logFile.exists()) try {
					logFile.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Affiche un message d'erreur. Si l'application est lancée en mode
	 * interface graphique, cela affichera une boite de dialogue, et,
	 * optionnellement, inscrira le message dans le fichier de log. Sinon,
	 * écrira juste le message d'erreur dans le fichier de log.
	 * 
	 * @param title
	 * Le titre du message.
	 * @param content
	 * Le contenu du message.
	 * @param showInLogFile
	 * Doit-on ajouter le message dans le fichier de log ? <u>N'est effectif
	 * qu'en mode interface graphique</u>. Si {@code true}, le message sera
	 * inscrit comme un message d'erreur.
	 */
	public static void sendErrorMessage(String title, String content, boolean showInLogFile)
	{
		if (EntryPoint.getIsGUIMode()) {
			JOptionPane.showMessageDialog(null, content, title, JOptionPane.ERROR_MESSAGE);
			if (showInLogFile) DebugManager.print(" * " + title + " * ", content, MESSAGE_ERROR);
		} else DebugManager.print(" * " + title + " * ", content, MESSAGE_ERROR);
	}
}
