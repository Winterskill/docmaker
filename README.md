# DocMaker

DocMaker est un générateur de documentation à partir de sources JSON.

Lien de téléchargement : [https://mega.nz/#!8PYEGQYS!-urvTOZh7kNwOAneFJ0aBmeZfEcjdrPT48b9ifuepv4](https://mega.nz/#!8PYEGQYS!-urvTOZh7kNwOAneFJ0aBmeZfEcjdrPT48b9ifuepv4)



# Changelog

## v1.1.0pre1
Sortie le :

### Ajouts

* Ajout d'une interface graphique basique

### Fixes

* Fix du bug du breadcrumb qui ne s'affichait pas dans l'ordre
* Fix du bug d'affichage des types des membres de classe quand le type n'est pas défini dans la documentation
* Fix du bug de non affichage des types quand le type n'est pas défini dans la documentatin

## v1.0.1
Sortie le : vendredi 4 janvier 2019

### Ajouts

* Ajout d'un fichier README.md
* Ajout de l'affichage de la date et de l'heure dans les fichiers de log
* Amélioration de la javadoc

### Fixes

* Fix du bug d'encodage des accents dans la javadoc
* Allègement de l'executable généré

## v1.0.0
Sortie le 24 décembre 2018

Version initiale, en ligne de commandes.